package hub.riadh.customerdataservice.web.graphql;

import hub.riadh.customerdataservice.dto.CustomerDto;
import hub.riadh.customerdataservice.entities.Customer;
import hub.riadh.customerdataservice.mapper.CustomerMapper;
import hub.riadh.customerdataservice.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
@AllArgsConstructor
public class CustomerGraphQLController {
    private CustomerRepository customerRepository;

    @QueryMapping
    public List<Customer> allCustomers() {
        return customerRepository.findAll();
    }


    @QueryMapping
    public Customer customerById(@Argument Long id) {

        Customer customer = customerRepository.findById(id).orElse(null);
        // normalement tous les traitement de verification se fait niveau service . mais pour raison de simplification on l'a mis ici
        if (customer == null) {
            throw new RuntimeException(String.format("Customer %s not found", id));
        }
        return customer;
    }

    @MutationMapping
    public Customer saveCustomer(@Argument CustomerDto customer) {
        Customer customerEntity = CustomerMapper.MAPPER.mapToCustomer(customer);
        return customerRepository.save(customerEntity);
    }

}
