package hub.riadh.customerdataservice.web.soap;

import hub.riadh.customerdataservice.dto.CustomerDto;
import hub.riadh.customerdataservice.entities.Customer;
import hub.riadh.customerdataservice.mapper.CustomerMapper;
import hub.riadh.customerdataservice.repository.CustomerRepository;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Component
@AllArgsConstructor
@WebService(serviceName = "CustomerWS")
public class CustomerSoapService {

//    http://localhost:8082/services/CustomerService?wsdl

    private CustomerRepository customerRepository;

    @WebMethod
    public List<Customer> customerList() {
        return customerRepository.findAll();
    }

    @WebMethod
    public Customer customerById(@WebParam(name = "id") Long id) {
        return customerRepository.findById(id).get();
    }

    @WebMethod
    public Customer saveCustomer(@WebParam(name = "customer") CustomerDto customer) {
        Customer customerEntity = CustomerMapper.MAPPER.mapToCustomer(customer);
        return customerRepository.save(customerEntity);
    }

}
