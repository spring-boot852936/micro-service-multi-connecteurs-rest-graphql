package hub.riadh.customerdataservice.config;

import hub.riadh.customerdataservice.web.soap.CustomerSoapService;
import lombok.AllArgsConstructor;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class CXFSoapWebServiceConfig {
    @Autowired
    private Bus bus;
    @Autowired
    private CustomerSoapService customerSoapService;

    @Bean
    public EndpointImpl endpoint(){
        EndpointImpl endpoint=new EndpointImpl(bus,customerSoapService);
        endpoint.publish("/CustomerService");
//        http://localhost:8082/services/CustomerService?wsdl
        return endpoint;
    }
}
