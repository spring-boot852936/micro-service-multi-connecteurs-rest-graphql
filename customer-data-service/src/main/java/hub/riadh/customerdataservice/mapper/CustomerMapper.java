package hub.riadh.customerdataservice.mapper;

import hub.riadh.customerdataservice.dto.CustomerDto;
import hub.riadh.customerdataservice.entities.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper()
public interface CustomerMapper {
    CustomerMapper MAPPER = Mappers.getMapper(CustomerMapper.class);

    CustomerDto mapToCustomerRequest(Customer customer);

    Customer mapToCustomer(CustomerDto userDto);

}
