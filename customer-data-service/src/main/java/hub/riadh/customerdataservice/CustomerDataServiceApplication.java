package hub.riadh.customerdataservice;

import hub.riadh.customerdataservice.entities.Customer;
import hub.riadh.customerdataservice.repository.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CustomerDataServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerDataServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(CustomerRepository customerRepository){
		return args -> {
			customerRepository.save(Customer.builder().name("Riadh").email("riadh@gmail.com").build());
			customerRepository.save(Customer.builder().name("Ameni").email("ameni@gmail.com").build());
			customerRepository.save(Customer.builder().name("Layan").email("layan@gmail.com").build());
		};
	}
}
