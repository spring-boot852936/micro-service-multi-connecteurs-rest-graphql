package hub.riadh.accountdataservice.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class Department {
    private long id;
    private String name;
    private List<Customer> customers;
    protected LocalDateTime createdDate;
    protected LocalDateTime lastModifiedDate;

}
