package hub.riadh.accountdataservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    private long id;
    private String name;
    private String email;
    private Department department;
    protected LocalDateTime createdDate;
    protected LocalDateTime lastModifiedDate;

}
