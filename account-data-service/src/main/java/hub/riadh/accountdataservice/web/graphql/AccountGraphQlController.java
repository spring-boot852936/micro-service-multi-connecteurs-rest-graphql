package hub.riadh.accountdataservice.web.graphql;

import hub.riadh.accountdataservice.feign.CustomerRestClient;
import hub.riadh.accountdataservice.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.client.HttpGraphQlClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/account-service/gql")

public class AccountGraphQlController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/customers")
    public Mono<List<Customer>> getListCustomers() {
        HttpGraphQlClient graphQlClient = HttpGraphQlClient.builder()
                .url("http://localhost:8082/graphql")
                .build();
        var httpRequestDocument = """
                 query {
                     allCustomers{
                       name,email, id
                     }
                   }
                """;
        Mono<List<Customer>> customers = graphQlClient.document(httpRequestDocument)
                .retrieve("allCustomers")
                .toEntityList(Customer.class);
        return customers;

    }

    @GetMapping("/customers/{id}")
    public Mono<Customer> getCustomerById(@PathVariable Long id) {
        HttpGraphQlClient graphQlClient = HttpGraphQlClient.builder()
                .url("http://localhost:8082/graphql")
                .build();
        var httpRequestDocument = """
                 query($id:Int) {
                    customerById(id:$id){
                      id, name, email
                    }
                  }
                """;
        Mono<Customer> customerById = graphQlClient.document(httpRequestDocument)
                .variable("id", id)
                .retrieve("customerById")
                .toEntity(Customer.class);
        return customerById;

    }
}
