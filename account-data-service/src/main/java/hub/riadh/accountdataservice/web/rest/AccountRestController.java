package hub.riadh.accountdataservice.web.rest;

import hub.riadh.accountdataservice.feign.CustomerRestClient;
import hub.riadh.accountdataservice.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/account-service")
public class AccountRestController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private CustomerRestClient customerRestClient;

    @GetMapping("/customers")
    public List<Customer> getListCustomers() {
        Customer[] customers = restTemplate.getForObject("http://localhost:8082/customers", Customer[].class);
        return List.of(customers);
    }

    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable Long id) {
        Customer customer = restTemplate.getForObject("http://localhost:8082/customer/" + id, Customer.class);
        return customer;
    }

    @GetMapping("/customers/v2")
    public Flux<Customer> getListCustomersv2() {
        // WebClient plus performant que RestTemplate
        WebClient webClient = WebClient.builder()
                .baseUrl("http://localhost:8082")
                .build();
        Flux<Customer> customersFLux = webClient.get().uri("/customers").retrieve().bodyToFlux(Customer.class);
        return customersFLux;
    }
    @GetMapping("/customers/v2/{id}")
    public Mono<Customer> getCustomerByIdV2(@PathVariable Long id) {
        WebClient webClient = WebClient.builder()
                .baseUrl("http://localhost:8082")
                .build();
        Mono<Customer> customersFLux = webClient.get().uri("/customers/"+id).retrieve().bodyToMono(Customer.class);
        return customersFLux;
    }

    @GetMapping("/customers/v3")
    public List<Customer> getListCustomersV3() {
        return customerRestClient.getCustomers();
    }

    @GetMapping("/customers/v3/{id}")
    public Customer getCustomerByIdV3(@PathVariable Long id) {
        return customerRestClient.getCustomerById(id);
    }

    @GetMapping("/customers/v4")
    public List<Customer> getListCustomersV4() {
        return customerRestClient.getCustomers();
    }

}
