package hub.riadh.accountdataservice.feign;

import hub.riadh.accountdataservice.model.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(url = "http://localhost:8082",value = "customer-rest-client")
public interface CustomerRestClient {
    @GetMapping("/customers")
    public List<Customer> getCustomers();
    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable Long id);
}
